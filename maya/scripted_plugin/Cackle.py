import cackle

def initializePlugin(mobject):
	cackle.load()

def uninitializePlugin(mobject):
	cackle.unload()
